FROM registry.gitlab.com/harukanetwork/oss/harukadockercenter:latest

ENV PATH="/app/bin:$PATH"
WORKDIR /app

RUN git clone https://gitlab.com/HarukaNetwork/OSS/Telegram-Paperplane.git -b staging /app

#
# Copies session and config(if it exists)
#
COPY ./sample_config.env ./userbot.session* ./config.env* ./client_secrets.json* ./secret.json* /app/

#
# Finalization
#
CMD ["bash","init/start.sh"]
